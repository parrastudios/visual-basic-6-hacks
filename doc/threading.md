### __Threading Documentation__
__How to allow threads in VB6__

Motivation:

Multiple available and safe options:
	- External DLL, LIB or TBL (could be done in VB6 or not)
	- Implicit in the executable (done directly with VB6 and Win32 API)

Explanation of methods:
	
	
Differences between methods:
	
	
Implementation of the hack:
	
	
Conclusion:
	Best aproach can differ depending on specifications we want.
	
	From my point of view, best option would be to implement it directly on the same program.
	This will be always more efficient, because of direct approach, but could be so hard to debug. The debugger of VB6 it's not allowed to do this, so we have to make some tricks to log program flow, or directly use a better debugger as ollydbg (<http://www.ollydbg.de/>).
	Another solution could be to make our on runtime debugger, to give support to handle our threading applications (described on debug section).
	
	We have to determine our interests and choose the best option.

References:
	- External LIB implementation with VB6:
		<http://www.planet-source-code.com/vb/scripts/ShowCode.asp?txtCodeId=24747&lngWId=1>
	- Specific VB6 issues related to the implementation of its VM (ActiveX EXE Server, interesting for writing pararell routines):
		<http://www.freevbcode.com/ShowCode.asp?ID=1287>
	- Another implementation of Activex EXE Server:
		<http://www.planet-source-code.com/vb/scripts/ShowCode.asp?txtCodeId=63601&lngWId=1>
	- Implementation with TBL:
		<http://www.planet-source-code.com/vb/scripts/ShowCode.asp?txtCodeId=36373&lngWId=1>
	- Implementation directly done on program (the hack):
		<http://www.planet-source-code.com/vb/scripts/ShowCode.asp?lngWId=1&txtCodeId=73905>
	- Documentation about implementation details and debugging:
		<https://www.microsoft.com/msj/0897/multithreading.aspx>
	