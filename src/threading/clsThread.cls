VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsThread"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
''  Visual Basic 6 Hacks by Parra Studios
''
''  A collection of sources that implement functionalities not allowed in this
''  language or low level performance improvements with its
''  respective tests and documentation.
''
''  Copyright (C) 2009-2014 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
''
''  This program is free software: you can redistribute it and/or modify
''  it under the terms of the GNU Affero General Public License as
''  published by the Free Software Foundation, either version 3 of the
''  License, or (at your option) any later version.
''
''  This program is distributed in the hope that it will be useful,
''  but WITHOUT ANY WARRANTY; without even the implied warranty of
''  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
''  GNU Affero General Public License for more details.
''
''  You should have received a copy of the GNU Affero General Public License
''  along with this program.  If not, see <http://www.gnu.org/licenses/> or
''  <http://www.affero.org/oagpl.html>.
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

Option Explicit

''
' Win API declarations

Private Const GWL_WNDPROC As Long = -4

Private Declare Function GetWindowLong Lib "user32" Alias "GetWindowLongA" (ByVal hWnd As Long, ByVal nIndex As Long) As Long

''
' Asynchronous Thread Event Pointers
'   - Raised by Main Thread (MT)
'   - Raised by Created Thread (CT)

Public Event TaskCallback(ByVal Data As Variant, Result As Variant)     ' (MT) Main thread function
Public Event ProgressCallback(ByVal Percent As Long)                    ' (CT) Raised when main progress changed
Public Event FinishCallback(ByVal Result As Variant)                    ' (CT) Raised when thread finishes

''
' Thread Status Ennumeration

Public Enum StatusEnum
    STATUS_INVALID = 0          ' Not initialized
    STATUS_SUSPENDED            ' Suspended
    STATUS_RUNNING              ' Running
    STATUS_TERMINATED           ' Finished
End Enum

''
' Thread Event Raised Flags Ennumeration

Public Enum EventRaisedFlagsEnum
    CALLBACK_INVALID_FLAG = 0   ' Not initialized
    CALLBACK_TASK_FLAG = 1      ' Task raised
    CALLBACK_PROGRESS_FLAG = 2  ' Progress changed raised
    CALLBACK_FINISH_FLAG = 4    ' Thread finished raised
End Enum

''
' Thread Parent Properties

Private Type ParentType
    From As Form
    WindowHandle As Long
    ThreadHandle As Long
End Type

''
' Thread Properties

Private Type ThreadImplType
    Id As Long
    Status As StatusEnum
    Cancelled As Boolean
    Progress As Long
    Data As Variant
    Handle As Long
    Parent As ParentType
    EventFlags As EventRaisedFlagsEnum
End Type

Private ThreadImpl As ThreadImplType

Private Sub Class_Initialize()
    Reset
End Sub

Private Sub Class_Terminate()
    If ThreadImpl.Status = STATUS_RUNNING Then
        
    ElseIf ThreadImpl.Status = STATUS_SUSPENDED Then
    
    End If
End Sub

Public Property Get Id() As Long
    Id = ThreadImpl.Id
End Property

Public Property Get Handle() As Long
    Handle = ThreadImpl.Handle
End Property

Public Property Get Data() As Variant
    Data = ThreadImpl.Data
End Property

Public Property Get Status() As StatusEnum
    Status = ThreadImpl.Status
End Property

Public Property Get Cancelled() As Boolean
    Cancelled = Not ThreadImpl.Cancelled
End Property

Public Sub Reset()
    Set ThreadImpl.Parent.From = Nothing
    
    ThreadImpl.Parent.ThreadHandle = 0
    ThreadImpl.Parent.WindowHandle = 0
    
    ThreadImpl.Id = 0
    ThreadImpl.Status = STATUS_INVALID
    ThreadImpl.Data = CVar(0)
    ThreadImpl.Progress = 0
    ThreadImpl.Cancelled = False
    ThreadImpl.EventFlags = CALLBACK_INVALID_FLAG
    ThreadImpl.Handle = 0
End Sub

Public Sub Launch()

End Sub

Public Sub Suspend()

End Sub

Public Sub Restart()

End Sub

Public Sub Cancel()
    ThreadImpl.Cancelled = True
End Sub

Private Sub Notify(ByVal Message As Long, Optional ByVal Async As Boolean = False)
    If Async Then
        PostMessage ThreadImpl.Parent.WindowHandle, Message, 0, ObjPtr(Me)
    Else
        SendMessage ThreadImpl.Parent.WindowHandle, Message, 0, ByVal ObjPtr(Me)
    End If
End Sub

Private Function AddressOfImpl(ByVal Address As Long) As Long
    AddressOfImpl = Address
End Function

Private Sub SubClassParent()
    modThread.ThreadRefCount = modThread.ThreadRefCount + 1
    
    If GetWindowLong(ThreadImpl.Parent.WindowHandle, GWL_WNDPROC) = AddressOfImpl(AddressOf NewWndProc) Then
        ' Already subclassed
        Exit Sub
    End If
    
    modThreading.FormWndProc = SetWindowLong(ThreadImpl.Parent.WindowHandle, GWL_WNDPROC, AddressOf NewWndProc)
End Sub

Private Sub UnSubClassParent()

End Sub
