![](https://bitbucket-assetroot.s3.amazonaws.com/c/photos/2014/Apr/15/visual-basic-6-hacks-logo-2346326447-2_avatar.png "Visual Basic 6 Hacks")
### __Visual Basic 6 Hacks__
__A collection of sources that implement functionalities not allowed in this language or low level performance improvements with its respective tests and documentation.__

- - -


__LICENSE__

Copyright (C) 2008 - 2014
Vicente Ferrer - Parra Studios (<parra_real_93@hotmail.com>)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>


![](http://i.creativecommons.org/l/by-nc-sa/3.0/88x31.png "Creative Commons License")

This work is licensed under a [Creative Commons Attribution-NonCommercial-ShareAlike 3.0 Unported License](http://creativecommons.org/licenses/by-nc-sa/3.0/ "License")
